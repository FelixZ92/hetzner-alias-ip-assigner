module gitlab.com/felixz92/hetzner-alias-ip-assigner

go 1.16

require (
	github.com/hetznercloud/hcloud-go v1.32.0
	github.com/spf13/pflag v1.0.5
)
