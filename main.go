package main

import (
	"context"
	"fmt"
	"github.com/hetznercloud/hcloud-go/hcloud"
	flag "github.com/spf13/pflag"
	"log"
	"net"
	"os/exec"
	"strconv"
)

const (
	Master Endstate = "MASTER"
	Backup Endstate = "BACKUP"
	Fault  Endstate = "FAULT"
)

type Endstate string

type Options struct {
	apiKey       string
	aliasIP      string
	serverName   string
	networkID    string
	ipBinPath    string
	device       string
	endstate     string
	peerSelector string
}

func (o *Options) Endstate() Endstate {
	return Endstate(o.endstate)
}

func (o *Options) BindFlags(fs *flag.FlagSet) {
	fs.StringVar(&o.apiKey, "api-key", "", "HCloud API Key")
	fs.StringVar(&o.aliasIP, "alias-ip", "", "Alias IP to use")
	fs.StringVar(&o.serverName, "server-name", "", "serverName")
	fs.StringVar(&o.networkID, "network-id", "", "network-id")
	fs.StringVar(&o.ipBinPath, "ip-bin-path", "/bin/ip", "path to ip binary")
	fs.StringVar(&o.device, "device", "ens10", "the interface to operate on")
	fs.StringVar(&o.endstate, "endstate", "", "the vrrp endstate (MASTER|BACKUP|FAULT)")
	fs.StringVar(&o.peerSelector, "peer-label-selector", "controlplane=1", "the hcloud label selector for our peers")
}

func (o *Options) validate() {
	if o.apiKey == "" {
		log.Fatalf("No API Key specified!")
	}

	if o.aliasIP == "" {
		log.Fatalf("No Floating IP specified!")
	}

	if o.serverName == "" {
		log.Fatalf("No server serverName specified!")
	}

	if o.networkID == "" {
		log.Fatalf("No network id specified!")
	}

	if o.endstate == "" {
		log.Fatalf("No endstate specified!")
	}
}

func runIpAddr(options Options, ip net.IP, command string) {
	fmt.Printf("ip = %s/32\n", ip.String())
	cmd := exec.Command(options.ipBinPath, "addr", command, fmt.Sprintf("%s/32", ip.String()), "dev", options.device)
	fmt.Printf("command = %s\n", cmd.String())
	if err := cmd.Run(); err != nil {
		log.Fatalf("error %s ip addr: %s\n", command, err)
	}
}

func delIpAddr(options Options, ip net.IP) {
	runIpAddr(options, ip, "del")
}

func addIpAddr(options Options, ip net.IP) {
	runIpAddr(options, ip, "add")
}

func main() {

	var options Options
	options.BindFlags(flag.CommandLine)
	flag.Parse()

	options.validate()

	nid, err := strconv.Atoi(options.networkID)
	if err != nil {
		log.Fatalf("could not parse network id: %s\n", err)
	}

	// Initialize HCloud Client
	client := hcloud.NewClient(hcloud.WithToken(options.apiKey))

	ip := net.ParseIP(options.aliasIP)

	if options.Endstate() == Master {
		removeAliasIPFromPeers(client, options, nid, ip)
		addAliasIPToMaster(client, options, nid, ip)
		addIpAddr(options, ip)
	} else {
		delIpAddr(options, ip)
	}
}

func addAliasIPToMaster(client *hcloud.Client, options Options, nid int, ip net.IP) {
	server, _, err := client.Server.GetByName(context.Background(), options.serverName)
	if err != nil {
		log.Fatalf("error retrieving server: %s\n", err)
	}

	if server != nil {
		networkIdx := getNetworkIdx(server, nid)
		aliases := append(server.PrivateNet[networkIdx].Aliases, ip)
		_, _, err := client.Server.ChangeAliasIPs(context.Background(), server, hcloud.ServerChangeAliasIPsOpts{
			Network:  server.PrivateNet[networkIdx].Network,
			AliasIPs: aliases,
		})
		if err != nil {
			log.Fatalf("error updating server: %s\n", err)
		}

	} else {
		fmt.Printf("server with serverName %v was not found!\n", options.serverName)
	}
}

func removeAliasIPFromPeers(client *hcloud.Client, options Options, nid int, ip net.IP) {
	peers, _, err := client.Server.List(context.Background(), hcloud.ServerListOpts{
		ListOpts: hcloud.ListOpts{LabelSelector: options.peerSelector},
	})

	if err != nil {
		log.Fatalf("error retrieving peers: %s\n", err)
	}

	for _, peer := range peers {
		if peer != nil {
			networkIdx := getNetworkIdx(peer, nid)

			aliases := Remove(peer.PrivateNet[networkIdx].Aliases, ip)

			_, r, err := client.Server.ChangeAliasIPs(context.Background(), peer, hcloud.ServerChangeAliasIPsOpts{
				Network:  peer.PrivateNet[networkIdx].Network,
				AliasIPs: aliases,
			})
			if err != nil {
				log.Fatalf("error updating server: %s\n", err)
			}

			fmt.Printf("response: %d\n", r.StatusCode)

		} else {
			fmt.Printf("server with serverName %v was not found!\n", options.serverName)
		}
	}
}

func getNetworkIdx(server *hcloud.Server, nid int) int {
	var networkIdx int
	fmt.Printf("server: %s\n", server.Name)
	for i, network := range server.PrivateNet {
		fmt.Printf("network name: %d\n", network.Network.ID)
		fmt.Printf("aliases: %v\n", network.Aliases)
		fmt.Printf("network: %v\n", network.Network)
		if network.Network.ID == nid {
			networkIdx = i
			break
		}
	}

	return networkIdx
}

func Contains(s []net.IP, value net.IP) bool {
	for _, ip := range s {
		if ip.Equal(value) {
			return true
		}
	}

	return false
}

func Remove(s []net.IP, value net.IP) []net.IP {
	for i, ip := range s {
		if ip.Equal(value) {
			return RemoveIndex(s, i)
		}
	}

	return s
}

func RemoveIndex(s []net.IP, index int) []net.IP {
	return append(s[:index], s[index+1:]...)
}
